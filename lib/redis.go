package lib

import (
	"github.com/garyburd/redigo/redis"
	"time"
)

func NewPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     50,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}

			if len(password) > 0 {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					return nil, err
				}
			}

			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

var Pool *redis.Pool

//var (
//	redisServer   = flag.String("redisServer", ":6379", "")
//	redisPassword = flag.String("redisPassword", "", "")
//)
